if GOENV=$(y7locate go/env "$GOENV" config)
then
    export GOENV
    y7_go="$GOENV"
    unset $(go env | sed -n -e '/^GO/s/=.*//p')
    export GOENV="$y7_go"
else
    y7psplit y7_go "$GOPATH"
    for y7_d in "$HOME/opt/go" "$HOME/go"
    do
        y7arrayadd y7_go -d "$y7_d"
    done
    GOPATH=$(y7pjoin "${y7_go[@]}")
    if [[ -n "$GOPATH" ]]
    then
        export GOPATH
    fi
fi
